package com.ml4ai.server.dto;

import com.ml4ai.server.dto.base.BaseAuditDTO;
import com.ml4ai.server.utils.annotation.QueryColumn;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 * Created by leecheng on 2018/9/24.
 */
@Data
public class AuthorityDTO extends BaseAuditDTO implements GrantedAuthority {

    private String authority;

    private String authorityName;

    private String authorityData;

    @QueryColumn(propName = "roles.id")
    private Long roleId;
}
