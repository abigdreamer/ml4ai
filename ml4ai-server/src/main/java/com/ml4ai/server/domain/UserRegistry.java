package com.ml4ai.server.domain;

import com.ml4ai.server.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/8.
 */
@Getter
@Setter
@Entity
@Table(name = "T_USER_REGISTRY")
@Inheritance(strategy = InheritanceType.JOINED)
public class UserRegistry extends BaseAuditEntity {

    @ManyToOne
    @JoinColumn(name = "c_user_id")
    private User user;

}
