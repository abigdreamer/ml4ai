package com.ml4ai.server.services;

import com.ml4ai.server.domain.Ticket;
import com.ml4ai.server.dto.TicketDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by uesr on 2018/9/16.
 */
public interface TicketService extends BaseService<Ticket, TicketDTO> {
}
