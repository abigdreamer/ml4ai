package com.ml4ai.server.services.providers

import akka.actor.ActorSystem
import com.ml4ai.server.services.RabbitmqService
import com.ml4ai.server.services.actors.RootActors
import com.ml4ai.server.services.actors.anys.FileStreamTaskStart
import com.ml4ai.server.services.capability._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateStreamCommandReceiver extends CommandReceiver {

  @Autowired var rabbitmqService: RabbitmqService = null

  override def execute(command: Command): Unit = {
    RootActors.root.actorSelection(RootActors.root./("file2queue")) ! FileStreamTaskStart(command.getCallback, Map("rabbitMQAgent" -> rabbitmqService.getRabbitMQAgent, "queue" -> command.getCallback, "file" -> ""))
  }

  override def getSupport() = "create.stream"
}
