import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { GlobalsProvider } from '../providers/globals/globals';
@Component({
  templateUrl: 'app.html'
})
export class ExtendsApp {

  rootPage: any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private globals: GlobalsProvider) {
    let _me = this;
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      _me.globals.root = _me;
    });
  }
}

