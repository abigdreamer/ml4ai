import { debug } from '../env/config'

const reducer =
    (
        state = {
            props: {}
        },
        action) => {
        switch (action.type) {
            case "action": {                    //触发action函数
                if (action.action) {
                    try {
                        return action.action(state)
                    } catch (e) {
                        throw `非法action操作,${e}`
                    }
                } else {
                    throw "非法action操作"
                }
            }
            case "update": {                    //更新props属性
                let props = state.props
                const newState = Object.assign({}, state)
                const newProps = Object.assign({}, props, action.props)
                newState["props"] = newProps
                newState["rand"] = Math.random()
                return newState
            }
            default: {                          //默认返回自身
                return state
            }
        }
        throw "发生错误!"
    }
export default reducer