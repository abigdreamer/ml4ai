import React from 'react'
import ReactMarkdown from 'react-markdown'
import NetProvider from '../../net/net'
import map from '../../../../ctl/mapper'
import { debug } from '../../../../env/config'

const Component = React.Component

class Markdown extends Component {

    constructor(props) {
        super(props)
    }

    componentWillMount() {
        if (!this.props[this.props.url]) {
            if (debug) {
                console.log("pull url " + this.props.url)
            }
            this.pull(this.props.url)
        }
    }

    componentDidMount() {
        if (!this.props[this.props.url]) {

            this.pull(this.props.url)
        }
    }

    componentWillReceiveProps(props) {
        if (!props[props.url]) {
            this.pull(props.url)
        }
    }

    pull(url) {
        const net = new NetProvider()
        net.get(url, (res) => {
            let dataKey = `${url}`
            const data = {
            }
            data[dataKey] = res['data']
            this.props.dispatch({
                type: 'update',
                props: data
            })
            this.props.dispatch({
                type: "action",
                action: (state) => {
                    let newState = Object.assign({}, state)
                    if (newState.taskCount) {
                        newState.taskCount--
                    }
                    return newState
                }
            })
        }, (error) => {
            let dataKey = `${url}`
            const data = {
            }
            data[dataKey] = ''
            this.props.dispatch({
                type: 'update',
                props: data
            })
            this.props.dispatch({
                type: "action",
                action: (state) => {
                    let newState = Object.assign({}, state)
                    if (newState.taskCount) {
                        newState.taskCount--
                    }
                    return newState
                }
            })
        }, this)
        if (debug) {
            console.log("加入url请求", url)
        }
        this.props.dispatch({
            type: "action",
            action: (state) => {
                let newState = Object.assign({}, state)
                if (newState.taskCount == undefined) {
                    newState.taskCount = 0
                }
                newState.taskCount++
                return newState
            }
        })
    }

    render() {
        return <div>
            <ReactMarkdown escapeHtml={false} skipHtml={false} unwrapDisallowed={false} source={this.props[this.props.url]}></ReactMarkdown>
        </div>
    }

}

export default map(Markdown)