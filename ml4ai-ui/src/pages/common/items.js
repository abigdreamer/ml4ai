import React from 'react';
import { Menu, Icon, Tree } from 'antd';

import { ajax } from '../../config.service';

const TreeNode = Tree.TreeNode;
const SubMenu = Menu.SubMenu;
const ItemGroup = Menu.ItemGroup;



/**标题，在顶部，接受name参数 */
class XTop extends React.Component {
    render() {
        return (
            <div style={{ ...(this.props.style ? this.props.style : {}), position: 'relative', display: 'block', height: "32px", width: '100%', top: "0px", left: "0px" }}>{this.props.children}</div>
        )
    }
}

/**底部工具样，只能在relative的容器里 */
class XBottom extends React.Component {
    render() {
        return (
            <div style={{ ...(this.props.style ? this.props.style : {}), position: 'absolute', bottom: "0px", backgroundColor: '#fff', display: 'block', height: "48px", width: '100%', left: "0px" }}>{this.props.children}</div>
        );
    }

}

/**菜单栏，接受属性url，为加载菜单的HTTP数据
 * keyField,key的属性
 * textField,text的属性
 * childrenField,children属性
 */
class XMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            menus: []
        };
        this.apiURL = props.url;
        this.keyField = props.keyField ? props.keyField : 'id';
        this.textField = props.textField ? props.textField : 'menuName';
        this.childrenField = props.childrenField ? props.childrenField : 'children';
    }

    componentDidMount() {
        this.loadMenu();
    }

    //加载菜单
    loadMenu() {
        let _me = this;
        ajax.postJson(this.apiURL, {}, (result) => {
            let menus = result.data;
            _me.setState({ menus: result.data });
        });
    }

    //通过菜单数据加载菜单
    getMenus(data) {
        let currentLevel = [];
        for (let i = 0; i < data.length; i++) {
            let dat = data[i];
            if (dat[this.childrenField] && dat[this.childrenField].length && dat[this.childrenField].length > 0) {
                currentLevel.push(
                    <SubMenu key={dat[this.keyField]} title={dat[this.textField]}>
                        {this.getMenus(dat[this.childrenField])}
                    </SubMenu>
                );
            } else {
                currentLevel.push(<Menu.Item data={dat} key={dat[this.keyField]}>{dat[this.textField]}</Menu.Item>);
            }
        }
        return currentLevel;
    }

    //构建
    render() {
        let menus = this.state.menus;
        let handler = this.props.handle ? this.props.handle : () => { };
        return (
            <Menu {...(this.props.properties ? this.props.properties : {})} onClick={handler}>
                {this.getMenus(menus)}
            </Menu >
        );
    }

}

//树形控件
class XTree extends React.Component {

    constructor(props) {
        super(props);
        this.state = { treeData: [] };
        this.keyField = props.keyField ? props.keyField : "id";
        this.valueField = props.valueField ? props.valueField : "text";
        this.childrenField = props.childrenField ? props.childrenField : "children";
        this.apiURL = props.apiURL;
        if (props.registry) {
            props.registry(this);
        }
    }

    componentDidMount() {
        this.loadData();
    }

    componentWillReceiveProps() {
        this.loadData();
    }

    //加载数据
    loadData() {
        let _me = this;
        ajax.postJson(this.apiURL, {}, (result) => {
            _me.setState({ treeData: result.data });
        });
    }

    getTree(treeData) {
        let treeRoot;
        let properties = this.props.properties ? this.props.properties : {};
        treeRoot = (<Tree {...properties}>{this.getTreeList(treeData)}</Tree>);
        return treeRoot;
    }

    getTreeList(treeData) {
        let subItems = [];
        for (let i = 0; i < treeData.length; i++) {
            let item = treeData[i];
            let key = item[this.keyField];
            let text = item[this.valueField];
            if (item[this.childrenField] && item[this.childrenField].length && item[this.childrenField].length > 0) {
                subItems.push(<TreeNode data={item} key={key} title={text}>{this.getTreeList(item[this.childrenField])}</TreeNode>);
            } else {
                subItems.push(<TreeNode data={item} key={key} title={text}></TreeNode>);
            }
        }
        return subItems;
    }

    render() {
        return this.getTree(this.state.treeData);
    }
}

export { XTree, XTop, XBottom, XMenu };
