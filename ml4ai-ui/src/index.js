import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const AppRouter = () => (
    <App />
);
ReactDOM.render(<AppRouter />, document.getElementById('root'))
registerServiceWorker();
