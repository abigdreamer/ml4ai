package com.ml4ai.core;

import com.ml4ai.common.Toolkit;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class IsolationUrlClassLoader extends ClassLoader {

    private ClassLoader childLoader;
    private URL[] urls;
    private Map<String, Class<?>> classPool = new LinkedHashMap<>();

    public IsolationUrlClassLoader(ClassLoader parent, URL... urls) {
        super(parent);
        this.urls = urls;
    }

    public IsolationUrlClassLoader(ClassLoader parent, ClassLoader childLoader, URL... urls) {
        super(parent);
        this.childLoader = childLoader;
        this.urls = urls;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return super.loadClass(name);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (classPool.containsKey(name)) {
            Class clazz = classPool.get(name);
            return clazz;
        } else {
            Class clazz = null;
            for (URL url : urls) {
                try {
                    String protocol = url.getProtocol();
                    if (protocol.toUpperCase().contains("FILE")) {
                        String file = url.getFile();
                        File fileDir = new File(file);
                        String source = name.replace(".", File.separator).concat(".class");
                        if (fileDir.isFile()) {
                            source = name.replace(".", "/").concat(".class");
                            ZipFile zipFile = new ZipFile(file);
                            ZipEntry entry = zipFile.getEntry(source);
                            if (entry != null) {
                                InputStream inputStream = zipFile.getInputStream(entry);
                                byte[] classBytes = Toolkit.ByteUtil.readFully(inputStream);
                                inputStream.close();
                                clazz = defineClass(name, classBytes, 0, classBytes.length);
                                break;
                            }
                        } else if (fileDir.isDirectory()) {
                            String fullPath = fileDir.getPath().concat(File.separator).concat(source);
                            File classFile = new File(fullPath);
                            if (classFile.isFile() && classFile.exists()) {
                                InputStream inputStream = new FileInputStream(classFile);
                                byte[] classBytes = Toolkit.ByteUtil.readFully(inputStream);
                                inputStream.close();
                                clazz = defineClass(name, classBytes, 0, classBytes.length);
                                break;
                            }
                        }
                    } else if (protocol.toUpperCase().contains("HTTP")) {
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("accept", "*/*");
                        conn.setDoInput(true);
                        conn.setDoOutput(false);
                        conn.connect();
                        InputStream inputStream = conn.getInputStream();
                        byte[] classBytes = Toolkit.ByteUtil.readFully(inputStream);
                        inputStream.close();
                        clazz = defineClass(name, classBytes, 0, classBytes.length);
                        break;
                    } else {

                    }
                } catch (Exception e) {
                    throw new ClassNotFoundException(String.format("加载：%s 过程中出错！", name));
                }
            }
            if (clazz != null) {
                classPool.put(name, clazz);
                return clazz;
            } else {
                if (childLoader != null) {
                    clazz = childLoader.loadClass(name);
                    if (clazz != null) {
                        classPool.put(name, clazz);
                        return clazz;
                    }
                }
                throw new ClassNotFoundException(name);
            }
        }
    }

    @Override
    protected Object getClassLoadingLock(String className) {
        return super.getClassLoadingLock(className);
    }

    @Override
    public URL getResource(String name) {
        return super.getResource(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        return super.getResources(name);
    }

    @Override
    protected URL findResource(String name) {
        return super.findResource(name);
    }

    @Override
    protected Enumeration<URL> findResources(String name) throws IOException {
        return super.findResources(name);
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        return super.getResourceAsStream(name);
    }

    @Override
    protected Package definePackage(String name, String specTitle, String specVersion, String specVendor, String implTitle, String implVersion, String implVendor, URL sealBase) throws IllegalArgumentException {
        return super.definePackage(name, specTitle, specVersion, specVendor, implTitle, implVersion, implVendor, sealBase);
    }

    @Override
    protected Package getPackage(String name) {
        return super.getPackage(name);
    }

    @Override
    protected Package[] getPackages() {
        return super.getPackages();
    }

    @Override
    protected String findLibrary(String libname) {
        return super.findLibrary(libname);
    }

    @Override
    public void setDefaultAssertionStatus(boolean enabled) {
        super.setDefaultAssertionStatus(enabled);
    }

    @Override
    public void setPackageAssertionStatus(String packageName, boolean enabled) {
        super.setPackageAssertionStatus(packageName, enabled);
    }

    @Override
    public void setClassAssertionStatus(String className, boolean enabled) {
        super.setClassAssertionStatus(className, enabled);
    }

    @Override
    public void clearAssertionStatus() {
        super.clearAssertionStatus();
    }
}
